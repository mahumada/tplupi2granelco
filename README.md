# Granelco TP 2 AHUMADA

## Idea
  Se creo un sistema de cajero automatico con base de datos local a traves de archivos de texto, con el cual se puede acceder a un sistema de movimientos financieros, y manejo administrativo de usuarios mediante funciones disponibles de clases. Realizado en Python3.

## Contenidos

### Ingreso al sistema

  Se puede acceder al sistema mediante el menu inicial, que nos pedira elegir numericamente entre distintas opciones. A traves del mismo menu se iran obteniendo bifuraciones que llevaran a nuestro objetivo especifico. 
  En un principio se puede acceder como *usuario* o como *operador*. 
  Los **usuarios** pueden ingresar mediante sus credenciales DNI/Tarjeta y PIN de seguridad al sistema, con un conteo de errores que al llegar a 3 comprometeran al usuario con un bloqueo solamente reparable por un *operador*. Si entran satisfactoriamente se resetean los conteos del usuario. Una vez dentro, se puede ingresar o retirar dinero del sistema, verificar fondos, movimientos, y cambiar el PIN de seguridad. 
  Los **operadores** deben ingresar con un nombre de usuario y contrasena generales del sistema _(admin - 12345)_ con los cuales podran acceder a las funciones administrativas. Entre las mismas se encuentran crear usuarios, desbloquearlos, arquear cajas, resetear stock, entre otras.

### Guardado de datos

  Los datos son guardados en archivos de texto categorizados por carpeta, y tienen distintos formatos. Hay una categoria de personas fisicas y una de personas juridicas. Los **usuarios** de ambas tienen el formato `id: nombre apellido dni fechaNacimiento profesion sexo tarjeta pin errores`, uno por linea, o, `<id: razonSocial CUIT fechaCreacion tarjeta pin errores>` segun la categoria. Cuando se crea un usuario tambien se genera un archivo de **movimientos**, el cual tiene el formato: 
  ```
  Saldo AR$
  Saldo U$D
  Movimiento 5
  Movimiento 4
  Movimiento 3
  Movimiento 2
  Movimiento 1
  ```
---

### Video

Se puede encontrar un video de 10min yendo a lo largo del funcionamiento del programa demostrando las distintas funciones que el mismo posee y como la base de datos interactua con la gui y su correcto linkeo. 
[LINK YOUTUBE](https://www.youtube.com/watch?v=M5drj3ubvvI)


### Codigo

Para ejecutar el codigo, utilizar el comando en terminal situado en el mismo directorio:
```linux
$ python3 ./main.py
```

## `Hecho por Manuel Ahumada 2021 7mo Electronica`

---

# Tecnicismos

## Archivos

Se utilizo la funcion open() de python para leer, escribir, y editar los archivos de texto de la base de datos. Esto fue el principal trabajo despues de la adaptacion de todo el codigo entre si. 

### Leer

```Python3
with open("personasJuridicas/users.txt", "r") as file:
      file.read()
```

### Escribir

```Python3
with open("personasFisicas/users.txt", "w") as newFile:
  newFile.writelines(lines)
```

### Editar

```Python3
with open("personasFisicas/users.txt", "r") as file:
  lines = file.readlines()
    for i in range(len(lines)):
      if lines[i][0] == id:
        lines[i] = lines[i][:-2] + "0" + "\n"
  with open("personasFisicas/users.txt", "w") as newFile:
    newFile.writelines(lines)
```
