from getpass import getpass
from user import user
from db import db
from juridico import juridico

class gui:
  def clearConsole():
    print('\033c')
    
  def admin(self):
    self.clearConsole()
    print("Entrada exitosa al sistema de admin")
    print("[1] Nuevo Usuario", "[2] Desbloquear PIN", "[3] Arqueo de cajas", "", "[0] Salir", "",  sep="\n")
    option = input()
    if option == "1":
      self.clearConsole()
      print("Elija tipo de usuario:", "[1] Persona fisica", "[2] Persona juridica", "", "[0] Salir", "", sep="\n")
      option = input()
      # Persona Fisica
      if option == "1":
        self.clearConsole()
        print("Nombre:")
        nombre = input()
        print("Apellido:")
        apellido = input()
        print("DNI:")
        dni = input()
        print("Fecha de nacimiento:")
        fechaNacimiento = input()
        print("Profesion:")
        profesion = input()
        print("Sexo:")
        sexo = input()
        print("Tarjeta:")
        tarjeta = input()
        print("PIN:")
        pin = input()
        errors = 0
        newUser = user(nombre, apellido, dni, fechaNacimiento, profesion, sexo, tarjeta, pin, errors)
        print(newUser)
        db.agregarUsuarioFisico(newUser.nombre, newUser.apellido, newUser.dni, newUser.fechaNacimiento, newUser.profesion, newUser.sexo, newUser.tarjeta, newUser.pin, newUser.errors)
      elif option == "2":
        self.clearConsole()
        print("Razon social:")
        razonSocial = input()
        print("CUIT:")
        cuit = input()
        print("Fecha de inicio:")
        fechaInicio = input()
        print("Rubro:")
        rubro = input()
        print("Tarjeta:")
        tarjeta = input()
        print("PIN:")
        pin = input()
        errors = 0
        newUser = juridico(razonSocial, cuit, fechaInicio, rubro, tarjeta, pin, errors)
        db.agregarUsuarioJuridico(newUser.razonSocial, newUser.cuit, newUser.fechaInicio, newUser.rubro, newUser.tarjeta, newUser.pin, newUser.errors)
    elif option == "2":
      self.clearConsole()
      print("Numero de tarjeta:")
      tarjeta = input()
      db.resolverError(tarjeta)
    elif option == "3":
      db.arqueoCajas()
  
  def usuarioFisico(self, id):
    with open("personasFisicas/users.txt", "r") as file:
      lines = file.readlines()
      for i in range(len(lines)):
        if lines[i][0] == id:
          lines[i] = lines[i][:-2] + "0" + "\n"
      with open("personasFisicas/users.txt", "w") as newFile:
          newFile.writelines(lines)
    self.clearConsole()
    print("Opciones de usuario:", "[1] Deposito de dinero", "[2] Retiro de dinero", "[3] Cambiar PIN", "[4] Ver movimientos", "", "[0] Salir", "", sep="\n")
    print("Saldo actual: ")
    db.obtenerSaldoFisico(id)
    option = input()
    if option == "1":
      self.clearConsole()
      print("Monto a ingresar: ", end="")
      monto = int(input())
      print("Divisa:  ars | usd ", end="\n")
      moneda = input()
      db.modificarDineroFisico(id, monto, moneda)
      self.clearConsole()
      print("Saldo actual:")
      db.obtenerSaldoFisico(id)
    elif option == "2":
      self.clearConsole()
      print("Monto a retirar: ", end="")
      monto = int(input())*(-1)
      print("Divisa:  ars | usd ", end="\n")
      moneda = input()
      db.modificarDineroFisico(id, monto, moneda)
      #self.clearConsole()
      print("Saldo actual:")
      db.obtenerSaldoFisico(id)
    elif option == "3":
      self.clearConsole()
      with open("personasFisicas/users.txt", "r") as file:
        lines = file.readlines()
        for i in range(len(lines)):
          if lines[i][0] == str(id):
            print("Nuevo PIN: ")
            newLine = lines[i].split(" ")
            newLine[8] = input()
            lines[i] = " ".join(newLine)
        with open("personasFisicas/users.txt", "w") as newFile:
          newFile.writelines(lines)
    elif option == "4":
      with open("personasFisicas/movimientos/" + str(id) + ".txt", "r") as file:
        i = 0
        self.clearConsole()
        print("Movimientos:\n\n")
        for line in file:
          if i >= 2 and not str(line) == "null":
            print(line)
          i += 1

  def usuarioJuridico(self, id):
    self.clearConsole()
    with open("personasJuridicas/users.txt", "r") as file:
      lines = file.readlines()
      for i in range(len(lines)):
        if lines[i][0] == id:
          lines[i] = lines[i][:-2] + "0" + "\n"
      with open("personasJuridicas/users.txt", "w") as newFile:
          newFile.writelines(lines)
    print("Opciones de usuario:", "[1] Deposito de dinero", "[2] Retiro de dinero", "[3] Transferencia", "[4] Ver movimientos", "", "[0] Salir", "", sep="\n")
    print("Saldo actual: ")
    db.obtenerSaldoJuridico(id)
    option = input()
    if option == "1":
      self.clearConsole()
      print("Monto a ingresar: ", end="")
      monto = int(input())
      print("Divisa:  ars | usd ", end="\n")
      moneda = input()
      db.modificarDineroJuridico(id, monto, moneda)
      self.clearConsole()
      print("Saldo actual:")
      db.obtenerSaldoJuridico(id)
    elif option == "2":
      self.clearConsole()
      print("Monto a retirar: ", end="")
      monto = int(input())*(-1)
      print("Divisa:  ars | usd ", end="\n")
      moneda = input()
      db.modificarDineroJuridico(id, monto, moneda)
      self.clearConsole()
      print("Saldo actual:")
      db.obtenerSaldoJuridico(id)

  def usuario(self):
    self.clearConsole()
    print("Tipo de usuario:", "[1] Persona fisica", "[2] Persona juridica", "", "[0] Salir", "", sep="\n")
    option = input()
    if option == "1":
      self.clearConsole()
      print("Metodo de ingreso:", "[1] DNI + PIN", "[2] Tarjeta + PIN", "", sep="\n")
      option = input()
      if option == "1":
        self.clearConsole()
        print("DNI: ", end="")
        dni = input()
        pin = getpass("PIN: ")
        self.clearConsole()
        with open("personasFisicas/users.txt", "r") as file:
          lines = file.readlines()
          flag = 1
          for line in lines:
            if line.split(" ")[3] == str(dni) and line.split(" ")[8] == str(pin):
              if line.split(" ")[9][:-1] >= str(3):
                print("BLOCKED USER, ir a caja")
                return
              self.usuarioFisico(self, line.split(" ")[0][:-1])
              flag = 0
          if flag == 1:
            db.addError(dni, format = "dni")
          
      elif option == "2":
        self.clearConsole()
        print("Tarjeta: ", end="")
        tarjeta = input()
        pin = getpass("PIN: ")
        self.clearConsole()
        with open("personasFisicas/users.txt", "r") as file:
          lines = file.readlines()
          flag = 1
          for line in lines:
            if line.split(" ")[7] == str(tarjeta) and line.split(" ")[8] == str(pin):
              self.usuarioFisico(self, line.split(" ")[0][:-1])
              flag = 0
          if flag == 1:
            db.addError(tarjeta, format = "tarjetaFisica")
    elif option == "2":
      self.clearConsole()
      print("Metodo de ingreso:", "[1] CUIT + PIN", "[2] Tarjeta + PIN", "", sep="\n")
      option = input()
      if option == "1":
        self.clearConsole()
        print("CUIT: ", end="")
        cuit = input()
        pin = getpass("PIN: ")
        self.clearConsole()
        with open("personasJuridicas/users.txt", "r") as file:
          lines = file.readlines()
          flag = 1
          for line in lines:
            if line.split(" ")[2] == str(cuit) and line.split(" ")[6] == str(pin):
              self.usuarioJuridico(self, line.split(" ")[0][:-1])
              flag = 0
          if flag == 1:
            db.addError(dni, format = "cuit")

      elif option == "2":
        self.clearConsole()
        print("Tarjeta: ", end="")
        tarjeta = input()
        pin = getpass("PIN: ")
        self.clearConsole()
        with open("personasJuridicas/users.txt", "r") as file:
          lines = file.readlines()
          flag = 1
          for line in lines:
            if line.split(" ")[5] == str(tarjeta) and line.split(" ")[6] == str(pin):
              self.usuarioJuridico(self, line.split(" ")[0][:-1])
              flag = 0
          if flag == 1:
            db.addError(dni, format = "tarjetaJuridica")

  def operador(self):
    print("Ingrese sus datos de operador:", "Nombre: ", sep="\n")
    if input() == "admin":
      if getpass() == "12345":
        self.admin(self)
      else:
        print("Password incorrecta")
    else:
      print("Nombre incorrecto")

  def bienvenida(self):
    print("Bienvenido a granelco!", "", "Seleccione su opcion:", "[1] Ingresar como usuario", "[2] Ingresar como operador", "", "[0] Salir", "", sep="\n")
    option = input()
    self.clearConsole()
    if option == "1":
      self.usuario(self)
    elif option == "2":
      self.operador(self)
    else:
      print("Goodbye!")