from datetime import date

class db:
  def agregarUsuarioFisico(nombre, apellido, dni, fechaNacimiento, profesion, sexo, tarjeta, pin, errors):
    print('\033c')
    with open("personasFisicas/users.txt", "r") as file:
      count = len(file.readlines())
      file.read()
    with open("personasFisicas/users.txt", "a") as file:
      file.write(str(count) + ":" + " " + nombre + " " + apellido + " " + dni + " " + fechaNacimiento + " " + profesion + " " + sexo + " " + tarjeta + " " + pin + " " + str(errors) + "\n")
    with open("personasFisicas/movimientos/" + str(count) + ".txt", "w") as file:
      file.write("0\n0\nnull\nnull\nnull\nnull\nnull\n")
      
  def agregarUsuarioJuridico(razonSocial, cuit, fechaInicio, rubro, tarjeta, pin, errors):
    print('\033c')
    with open("personasJuridicas/users.txt", "r") as file:
      count = len(file.readlines())
      file.read()
    with open("personasJuridicas/users.txt", "a") as file:
      file.write(str(count) + ":" + " " + razonSocial + " " + cuit + " " + fechaInicio + " " + rubro + " " + tarjeta + " " + pin + " " + str(errors) + "\n")
    with open("personasJuridicas/movimientos/" + str(count) + ".txt", "w") as file:
      file.write("0\n0\nnull\nnull\nnull\nnull\nnull\n")
  
  def obtenerStockARS():
    with open("dinero/ars.txt", "r") as file:
      lista = []
      precios = [10, 20, 50, 100, 200, 500, 1000]
      suma = 0
      for line in file:
        lista.append(int(line[:-1]))
      for i in range(len(precios)):
        suma += lista[i] * precios[i]
      print(lista, suma)

  def obtenerStockUSD():
    with open("dinero/usd.txt", "r") as file:
      lista = []
      precios = [1, 2, 5, 10, 20, 50, 100]
      suma = 0
      for line in file:
        lista.append(int(line[:-1]))
      for i in range(len(precios)):
        suma += lista[i] * precios[i]
      print(lista, suma)

  def obtenerSaldoFisico(id):
    with open("personasFisicas/movimientos/" + str(id) + ".txt", "r") as file:
      i = 0
      for line in file:
        if i == 0:
          line1 = int(line[:-1])
          print("AR$" + line)
        elif i == 1:
          line2 = int(line[:-1])
          print("U$D" + line)
        elif i >= 2:
          # Return [AR$, U$D]
          return [line1, line2]
          break
        i += 1

  def obtenerSaldoJuridico(id):
    with open("personasJuridicas/movimientos/" + str(id) + ".txt", "r") as file:
      i = 0
      for line in file:
        if i == 0:
          line1 = int(line[:-1])
          print("AR$" + line)
        elif i == 1:
          line2 = int(line[:-1])
          print("U$D" + line)
        elif i >= 2:
          # Return [AR$, U$D]
          return [line1, line2]
          break
        i += 1

  def calcularBilletes(monto, divisa):
    billetes = [0, 0, 0, 0, 0, 0, 0]
    if divisa == "ars":
      precios = [1000, 500, 200, 100, 20, 50, 10]
    elif divisa == "usd":
      precios = [100, 50, 20, 10, 2, 5, 1]
    while True:
      if monto == 0:
        with open("dinero/" + str(divisa) + ".txt", "r") as file: 
          lines = file.readlines()
          for i in range(len(billetes)):
            lines[i] = str(int(lines[i][:-1]) - int(billetes[i])) + "\n"
          with open("dinero/" + str(divisa) + ".txt", "w") as newFile:
            newFile.writelines(lines)
        return True
      flag = 1
      for i in range(len(precios)):
        if precios[i] + monto <= 0:
          monto += precios[i]
          billetes[i] += 1
          flag = 0
          break
      if flag == 1:
        return "error"
      

  def modificarDineroFisico(id, cantidad, divisa):
    if int(cantidad) < 0:
      if db.calcularBilletes(cantidad, divisa) == "error":
        print("No hay billetes que cubran tu operacion exacta")
        return 
    with open("personasFisicas/movimientos/" + str(id) + ".txt", "r") as file:
      linesArray = file.readlines()
      if divisa == "ars":
        index = 0
      elif divisa == "usd":
        index = 1
      else:
        print("ERROR: Divisa invalida")
      linesArray[index] = str(int(linesArray[index]) + int(cantidad)) + "\n"
      with open("personasFisicas/movimientos/" + str(id) + ".txt", "w") as fileB:
        fileB.writelines(linesArray)
    with open("personasFisicas/movimientos/" + str(id) + ".txt", "r") as file:
      linesArray = file.readlines()
      for i in range(2, 6):
        print(i)
        linesArray[i] = linesArray[i+1]
      linesArray[6] = "Saldo " + str(cantidad) + str(divisa) + " " + str(date.today()) + "\n"
      with open("personasFisicas/movimientos/" + str(id) + ".txt", "w") as fileB:
        fileB.writelines(linesArray)

  def modificarDineroJuridico(id, cantidad, divisa):
    if int(cantidad) < 0:
      if db.calcularBilletes(cantidad, divisa) == "error":
        print("No hay billetes que cubran tu operacion exacta")
        return 
    with open("personasJuridicas/movimientos/" + str(id) + ".txt", "r") as file:
      linesArray = file.readlines()
      if divisa == "ars":
        index = 0
      elif divisa == "usd":
        index = 1
      else:
        print("ERROR: Divisa invalida")
      linesArray[index] = str(int(linesArray[index]) + int(cantidad)) + "\n"
      with open("personasJuridicas/movimientos/" + str(id) + ".txt", "w") as fileB:
        fileB.writelines(linesArray)
    with open("personasJuridicas/movimientos/" + str(id) + ".txt", "r") as file:
      linesArray = file.readlines()
      for i in range(2, 6):
        print(i)
        linesArray[i] = linesArray[i+1]
      linesArray[6] = "Saldo " + str(cantidad) + str(divisa) + " " + str(date.today()) + "\n"
      with open("personasJuridicas/movimientos/" + str(id) + ".txt", "w") as fileB:
        fileB.writelines(linesArray)

  def addError(value, format):
    if format == "dni":
      with open("personasFisicas/users.txt", "r") as file:
        lines = file.readlines()
        for i in range(len(lines)):
          if str(value) in lines[i]:
            lines[i] = lines[i][:-2] + str(int(lines[i][-2])+1) + "\n"
        with open("personasFisicas/users.txt", "w") as newFile:
          newFile.writelines(lines)

  def resolverError(tarjeta):
    with open("personasFisicas/users.txt", "r") as file:
      lines = file.readlines()
      for i in range(len(lines)):
        if str(tarjeta) in lines[i]:
          lines[i] = lines[i][:-2] + "0" + "\n"
      with open("personasFisicas/users.txt", "w") as newFile:
          newFile.writelines(lines)
    with open("personasJuridicas/users.txt", "r") as file:
      lines = file.readlines()
      for i in range(len(lines)):
        if str(tarjeta) in lines[i]:
          lines[i] = lines[i][:-2] + "0" + "\n"
      with open("personasJuridicas/users.txt", "w") as newFile:
          newFile.writelines(lines)

  def arqueoCajas():
    with open("dinero/ars.txt", "w") as file:
      file.write("10\n10\n10\n10\n10\n10\n10\n")
    with open("dinero/usd.txt", "w") as file:
      file.write("10\n10\n10\n10\n10\n10\n10\n")